---
title: '"Si ces concepts étaient plus populaires à gauche"'
created: 1614894972
modified: 1614898542
---

> Organizer en Pennsylvanie, ancien d’Occupy Wall Street, auteur de Hegemony How-To. À Roadmap for Radicals (AK Press, Oakland, 2017), Jonathan Matthew Smucker a publié le 26 mai 2019 sur son compte Twitter (@jonathansmucker) un long fil de conseils aux mouvements pour construire du pouvoir et contester l'hégémonie culturelle aux néolibéraux et aux conservateurs
> (Source : https://twitter.com/jonathansmucker/status/1132745954573127680).

1. Même si vous avez raison, vous ne gagnerez rien sans pouvoir.
2. Si vous ne choisissez pas vos batailles, vos opposants les choisiront pour vous.
3. Dans le contexte des batailles politiques, la discipline collective l'au moins aussi importante que l'autonomie.
4. La politique [...], c'est ce que vous faites. Si votre politique n'implique pas de construire et d'exercer un pouvoir collectif, ce n'est pas de la politique.
5. L'organizing - au sens classique d'organiser sur le terrain — signifie organiser des personnes au sein d'une force politique unie, en mesure de contester le pouvoir.

6. Planifier un événement ou lancer un groupe sur Facebook de « camarades » qui pensent la même chose peut être bien, mais ce n'est pas pareil.

7. La proposition : « La classe dominante, unie, ne sera jamais vaincue », est probablement plus vraie que : « Le peuple, uni, ne sera jamais vaincu ». Les fissures au sein des élites sont importantes pour notre succès et l'unité de notre base.

8. Fuir les méta-récits universalisants ne vous protégera pas d'être inscrit dans ceux de vos adversaires.

9. Notre tâche n'est pas de bâtir à partir de rien une sphère dédiée pour héberger des « activistes » socialement éclairés ou woke [conscients des enjeux de justice sociale]. Notre tâche est d'aligner et de politiser les espaces sociaux du quotidien, de tisser de la politique et de l'action collective dans la trame de la société.

10. Savoir ce qui ne va pas dans le système social et savoir comment changer le système sont deux catégories différentes de savoir. (Trop de personnes portant un regard critique ont du mal à saisir que posséder le premier ne leur confère pas automatiquement le second)

11. La révélation des méfaits des puissants n'entraîne que la résignation populaire s'il n'y a pas de contre-pouvoir viable pour tirer avantage de cette ouverture.

12. Vous pouvez développer une critique politique robuste sans vous occuper des questions de pouvoir et de stratégie. Mais vous ne pourrez pas créer ainsi une opération politique robuste.

13. Un leader sans base sociale n'est pas un leader. Les leaders efficaces émergent en tandem avec des organisations, des campagnes, des mouvements.

14. Le woke signaling [faire savoir que l'on est conscient des enjeux de justice sociale] est une version éclairée de l'élitisme.

15. Les politiques ne se préoccupent de l'opinion publique que lorsqu'elle est organisée.

16. Organiser une force politique populaire requiert de rencontrer les gens où ils sont, dans les espaces qu'ils fréquentent, en utilisant un langage courant. Les club-houses, eux, requièrent que les gens apprennent un vocabulaire spécial et s'assimilent à la sous-culture s'ils veulent rejoindre le club. Est-ce l'un ou l'autre que vous êtes en train de construire ?

17. Il est toujours peu avisé de sous-estimer les forces de vos opposants. Mais il est bien pire de croire que vos opposants sont tout puissants, invincibles et monolithiques, comme s'ils étaient destinés à gagner.

18. Les alliances temporaires sont une nécessité stratégique. Elles sont la norme, pas l'exception. Pour faire grandir nos forces et gagner, nous ne pouvons pas écarter des alliés prêts à nous rejoindre parce qu'ils n'étaient pas avec nous dans le passé (ou parce que nous pensons qu'ils vont nous la faire à l'envers dans l'avenir). « Pas d'ennemis permanents ni d'amis permanents, seulement des intérêts permanents. »
19. Nous pouvons nous plaindre autant que nous voulons des amis peu fiables et des alliés éphémères qui nous trahissent ou trahissent nos mouvements. Nos griefs peuvent être justifiés. Mais [...] c'est à nous de construire le pouvoir, les incitations et les moyens de dissuasion pour les empêcher d'agir de la sorte.

20. La règle des 80/20 : [consacrez] 80 % de votre temps à travailler à engager une base plus large pour créer de façon stratégique un « momentum » favorable qui permet de se projeter en avant. Et 20 % de votre temps à limiter les dégâts, à maîtriser les bêtises ou la toxicité, J'ai mis douze ans pour réaliser que mon ratio personnel était inversé.

21. Il y a une idée erronée dans certaines parties de la gauche selon laquelle vous modifiez la fenêtre d'Overton [un concept qui désigne les idées, les opinions qui paraissent à un moment donné « acceptables » et audibles] en disant des choses qui vont sembler extrêmes à la plupart des gens. En réalité, vous la modifiez si vous présentez des demandes osées comme des demandes populaires. Si vous présentez vos valeurs et votre agenda comme le sens commun.

22. Une insurrection n'est pas progressiste de façon inhérente, et l'hégémonie n'est pas réactionnaire de façon inhérente. Des politiques insurgées peuvent être de droite ou de gauche. Les élites peuvent être hégémoniques… mais les travailleurs aussi. Tout dépend de l'agenda des gouvernants. Et de celui des mutins.
