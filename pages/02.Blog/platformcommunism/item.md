---
title: 'Plutôt "communisme de plateforme" ?'
published: false
---

# Remplacer Airbnb, Uber et Deliveroo ?

Si on veut se la péter on pourrait utiliser l'expression "communisme de plateforme"

## Les plateformes ont le monopole : on fait quoi maintenant ?

"Airbnb est la plus grande entreprise d'hébergement, mais ne possède aucun bien immobilier, Uber la plus grande entreprise de taxi mais ne possède aucune voiture, Deliveroo celle de livraison sans aucune moto", etc.

<!-- communismedeplateforme -->
<!-- # I. De grands chantiers pour inventer "des communismes (écolos) de palteforme" et en faire le grand nouveau défi intellectuel -->

Ces plateformes sont dominantes, certes par leur interface polie et optimisée, mais surtout parce que leur modèle de financement permet de fonctionner à perte pendant des années jusqu'à pouvoir couler la concurrence et remplacer le marché (comme l'analyse si bien Evgeny Morozov dans l'article [Uber ou le miroir de l'impuissance publique](https://blog.mondediplo.net/2016-02-01-Uber-miroir-de-l-impuissance-publique), qui précise de plus que cette force de frappe économique est issue de milliards de dollars, ceux de Goldman Sachs placés dans des paradis fiscaux ou bien ceux des Saoudiens avec leur fonds d'investissement <!-- (CHECKER NOM DU FONDS D'INVESTISSEMENT UBER SAOUDIEN --> ), comme la stratégie bien connue d'Amazon

<!-- FIXME: expliquer sa stratégie -->

. Paypal est aussi simplement une offre hégémonique et tire ses profits presque uniquement de cette situation.

## Des alternatives en gestation

A côté de cela, des petites alternatives comme Coopcycle proposent à des livreur-ses salarié-es et organisé-es dans une coopérative d'utiliser leur logiciel qui permet de les mettre en relation avec des restaurants et entreprises, et des client-es. <!-- [A VERIFIER POUR LES CLIENTS] -->
Bien qu'elle ait obtenu des prix et l'encouragement de la Mairie de Paris, et qu'elle grandisse d'année en année, Coopcycle serait bien en peine de concurrencer Deliveroo qui ne paye aucune cotisation patronale et assure parfois le prix de sa course à perte, pour gagner des parts de marché.
La vente à perte est interdite mais quel législateur-ice frileux-se interdirait Deliveroo ou Uber ? Comment le prouver ? Et comment détricoter ces abus de position dominante de la part de ces entreprises ? Leur optimisation fiscale, quand même l'Irlande ne veut pas infliger des amendes record à ses entreprises, amendes qui iraient pourtant dans sa poche ?

Pour résumé, il existe des plateformes qui peuvent remplacer Deliveroo, etc. sur des bases plus saines. Par exemple Coopcycle, où des coopératives de salarié-es font de la livraison. Mais concurrencer Deliveroo à armes égales, c'est **jouer un jeu perdu d'avance : les plateformes sont simplement trop riches et peuvent se financer à perte**.

### Sur quoi d'autre pourraient-elles compter ?

Eh bien sur tout un ensemble de structures qui se chargeraient de la défense politique de ce modèle : partis politiques, mairies, départements, régions, mais aussi associations d'habitant-es, syndicats, coopératives d'usager-es, associations tout court, activistes.

Beaucoup sont déjà convaincu-es de l'épuisement du modèle startup, ou des VC (Venture Capitalists) financent au hasard des business plans au concept plus ou moins sensé dans l'espoir de tomber sur la _licorne_ qui compensera amplement leurs pertes.
Morozov cite une start-up finnoise, Kutsuplus, qui promettait un service constitué de navettes de bus que l'on pourrait commander par téléphone et dont le trajet serait déterminé par algorithme. <!-- COMPLETER DESC. MAIRIE ? -->
La conclusion de l'article est que si Kutsuplus n'a pas pu être financé, c'est que les milliards d'euros investissables ne sont plus dans les caisses de l'Etat mais dans les caisses d'immenses multinationales.

## Solution légale, illégale, ou zone grise ?

1. légalement, par des pouvoirs publics étatiques ou municipaux
   Si les partis sociaux-démocrates veulent financer ces alternatives, c'est tant mieux. Mais si elles ne le veulent pas, que faire ?
   Autrement dit, comment **établir sur un territoire donné** ces plateformes ?
2. ou extra-légalement par de l'auto-organisation à échelle locale (par exemple, via des modes d'action de lutte syndicale, comme la façon qu'a eu le Collectif des Livreur-ses Autogéré de Paris de faire fermer leur app Deliveroo aux restaurants pendant une grève en manifestant devant, puis en vérifiant via la même app l'application de cette consigne de grève des restaurants sur une zone donnée)

En fait, il faudrait pouvoir lutter contre avec plus d'armes, quitte à être dans l'illégal mais dans le légitime. Quand la technologie permet quelque chose d'illégal comme le piratage de musique, ça ne choque pas. Parasiter Deliveroo pour forcer son remplacement par des alternatives plus justes, même par des actions illégales comme des blocages, ça devrait être légitime.

Pour ce qui a trait au biens matériels : Airbnb, Uber, Deliveroo, illégalité veut dire action directe.
Pour Netflix, Spotify, Youtube, Instagram et le reste, cela veut dire solutions geek de clandestinité mais virtuel. (Instagram : adversarial interoperability. Youtube + Spotify : youtube-dl et spotify-dl. Pour Netflix, un Popcorn Time qui tient le coup des demandes de DMCA)

## Les liens avec le municipalisme

On fait :

- sur une zone : enforcement de technostructures (ex: coopcycle obligatoire et uber illégal), et de lois, et gouvernement de municipalisme libertaire selon un principe de subsidiarité..
- des centre-villes dont les emplacements publics et commerciaux sont affectés selon des critères de solidarité, de convivialité, de qulité et de vie et de travail et de petite échelle
- nationalisation / communisation ambitieuse du logement avec algorithes de répartition dont la mise en place est faite par les communautés organisées en zone

Cette coop type Coopcycle peut voir son usage s'imposer dû à de la désobéissance civile et une guerre de légitimité gagnée, ou être appuyée par l'Etat (ou la mairie, potentiellement elle aussi lancée dans une guerilla juridique de "désobéissance" vis-à-vis des règles de libre concurrence de l'Union Européenne). pour sa survie et son développement. La présence de ce bout de technostructure (un algorithme de matching à la Deliveroo) est agressive : elle essaie via action directe et cadre législatif d'empêcher d'autres acteurs de se développer sur un territoire.
On peut aussi imaginer que soit déclarée une loi obéie par certain-es acteur-ices qui conduirait à respecter un autre ensemble de pratiques socio-économico-techno-législatives, toujours sur un territoire donné (le territoire pouvant ne pas être contigu).

Devient à la fois action directe et programme politique qui peut se défendre à tous les niveaux : local, régional, national et européen.

### Coopératives d'achats et labels, avec pression pour l'achat d'un label

cf. labels syndicaux : _Le Crom, Jean-Pierre. "Le label syndical." (2004)._

### Transition :

Créer une culture associée à ces pratiques : embaucher les Deliveroo dans des mécanismes de coopératives et d'activité syndicale de lutte, etc.

# Un chantier de code aux décisions si vastes qu'il impliquerait une grande partie de la société

La revanche que peut apporter des projets technologiques politisés est extrêmement vaste <!-- (**A linker avec les listes de chantiers de code présents dans le pad [[orgs.Etape-2.md]]**)-->, par ailleurs [débattre du degré de politisation de ces technologies](https://repeindre.info/2020/02/14/contre-le-hackerisme-pt-1/) aussi (le web décentralisé en fait-il partie ? et Wikipédia ?) <!-- **liens articles Aaron Swartz** -->

<!-- ## Quelles propriétés à respecter ? Subsidiarité, fédérations de coopératives, anarcho-syndicalisme, éthique au travail, non mise en concurrence

- Fédérations de coopératives : le cas de coopcycle
- Subsidiarité : activité économique de livraison au plus proche des vies de quartier ? Ou non ? Les bobos se livrent des plats dans les quartiers riches et les livreur-ses sont pauvres. Est-ce désirable de continuer quand même ? Que faire ?
- Luttes des classe et anarcho syndicalisme : euh j'en sais rien mais ça fait classe ?
 -->

## Quelles qualités politiques pour des alternatives légitimes

1. la subsidiarité (plus petites sous-structures possible),
2. la fédération (agglomération de l'offre),
3. le _matching_ juste (entre offre et demande à l'échelle du réseau) et sans pression à la baisse des prix (pas de concurrence classique)

<!-- Sur le matching : imago pour démocratie et Valueflows pour système de valeur basé sur ensemble de valeurs plutôt que juste argent [[code.imago.md]] -->

## Quelques exemples de possibilités

### Netflix

- Popcorn Time in the browser (avec modèle éco de la diffusion : une taxe et un prorata à l'écoute ou au like ? avec des power voters ?)

### Spotify

- Funkwhale x Soulseek/Torrent (avec modèle éco de la diffusion : une taxe et un prorata à l'écoute ou au like ? avec des power voters ?)

### Airbnb

- Fairbnb mais en fédéré

### Uber

- Kutsuplus, en décentralisé et fédéré + offre de taxis à la Coopcycle

### Deliveroo

- [Coopcycle](https://coopcycle.org/fr/#about) 

<!-- ### Infinite examples

=> [[orgs.Etape-2.md]]

# Ouverture

- Baser ça sur la coopérative as a service d'axel et des dev avec une chouette et solide culture dev
- Devoir penser la question de la doocratie : c'est pas parce que les devs peuvent qu'ils (ils!) doivent faire, à penser en collectivité dans des organisations politiques et ne pas nier les rapports de force qui 's'opéreront entre chaque étape de la conception (impératifs d'efficacité, rentabilité, usabilité, respect de normes de travail, salaires, positions smboliques dans ce processus de décision, intérêts de classe, etc.)
- - que faire de la question du logement ? de la police ?
- [[laws.programme]]
- [[orgs.Etape-2.md]]

[[eco.coops.md]]

[[orgs.md]]
[[code.orgs.md]]
[[orgs.Etape-2.md]] -->

<!-- https://france.attac.org/nos-publications/les-possibles/numero-24-ete-2020/dossier-la-transformation-du-systeme-productif/article/democratiser-le-travail-dans-un-processus-de-revolution-ecologique-et-sociale#B-A-droite-et-a-gauche-du-Green-New-Deal -->

# Guerrilla économique ?

Limite faudrait écrire "manuel de guerrilla economique anarchistes par la coopération et la syndication"
Ou juste "principes d'economie coopérative entre anarchisme et syndicaliste"

## Concurrence impossible

Comment tu veux concurrencer les entreprises classiques qui sous traitent à des autoentrepreneurs quand tu essayes d'être plus éthique par exemple en payant des cotisations et en refusant de faire certains travaux.
Si on parle d'un secteur moins qualifié y a pas de faille le capitalisme est huilé et la compétition rude.

### avantage des coops

Ça peut marcher que dans des trucs en mode l'informatique ultra qualifiée où c'est des gens stylés trouvables que dans les co-ops car boycottent les entreprises classiques.
Ça dépend si les clients veulent être un truc cheap fait rapidement ou un truc bien et partenariat long terme du coup.
Et tout simplement on ne paie pas un salaire mirobolant de dirigeant donc la valeur ne s'enfuit pas on ne sait où.

## Guerrilla économique

Mais je trouve qu'on parle pas assez dans l'attaque justement d'oser d'être dans une zone grise juridique économique, d'être pas illegal mais agressif la dessus (par exemple la combinaison coopérative avec syndicat qui fait des actions contre les concurrents capitalistes), d'actions légitimes en disant : c'est ça la nouvelle loi qu'il faut mettre en place donc on l'applique en tant que syndicat car légitime et le corps législatif ne suit pas

### Mafia mais des gentils

Travailler a l'offensive économique en participant a des prises de marché offensive avec pourquoi pas des grèves "importunes"
Avec des deliveroo qui rackettent les restaurants qui veulent pas passer à coopcycle hahaha
#mafiamaisdesgentils

### syndicats et coopératives d'usager-es

- syndic consomateur/usagers + syndic classique
- embarquer les syndicats dans une nouvelle offensive
- ou juste en favorisant l'accompagnement des gens via les syndic dans les coop ayant des standards de vie au travail :ok_hand:

<!-- cf. texte d'Axel -->

## Coop d'usager-es et labels syndicaux

Et du coup la partie co-op d'usagers fait penser à l'histoire du label ouvrier.
<!-- émission super sur l'histoire des coopératives de production et coopératives d'usagers -->
Genre faire un label d'éthique dans le numérique bien socedem avec les gens de onestlatech, qui conduirait à dire qu'il faut être usager-e des coops.

## éducation étudiante coopérative

où les étudiant-es sont sociétaires de la coopérative pendant leurs etudes, pour éviter l'après fac "sans taf".

## Coops à la place des businesses de rente

- Que des co-ops appuyées par l'état récupère tous les business "rentes" : boutiques dans aéroports, snacks dans administrations et hôpitaux

## Les coops as a service

créer rapidement l'admin, gestion, compta, juridique optimisée pour une coop de tant de personnes, avec "optimisation fiscale" (autoentreperneur + structure coop + asso, comme le Puy du Fou haha) ou non.

## Ressources

- http://recma.org/actualite/quebec-brochure-syndicale-sur-les-cooperatives-en-milieu-de-travail
- http://recma.org/actualite/syndicalisme-et-ess-lexperience-quebecoise
- https://france.attac.org/nos-publications/les-possibles/numero-24-ete-2020/dossier-la-transformation-du-systeme-productif/article/democratiser-le-travail-dans-un-processus-de-revolution-ecologique-et-sociale#B-A-droite-et-a-gauche-du-Green-New-Deal
- https://www.hors-serie.net/Aux-Ressources/2020-06-13/Au-dela-de-la-propriete-id402