---
title: 'Former des porte-paroles pour propager des récits'
published: false
subtitle: 'Tu n''aimes pas Philippe Poutou, maintenant quoi ?'
modified: 1614642559
---

<!-- Note community organizing : dire que les bobos de La Base doivent être les salarié-es qui avec abnégation ne font que faire émerger et soutenir les leaders en faisant les taches ingrates et d'orga en retrait, importer le community organizing américain en france -->

<!-- Notes Léa : en gros en train d'inventer les porte-paroles ?
Oui, mais porte-paroles pour quelles organisations ? -->

<!-- Note : pas la même chose de faire justice dems/brnad new congress et de sortir des nouveaux candidats que de faire juste sortir des leaders qui parlent bien à la sunrise movement -->

<!-- cf. https://www.contretemps.eu/gauche-ocasio-cortez-aoc-sanders-socialisme-usa/ -->

# Former des porte-paroles pour propager des récits

<!-- # Une armée de nouveaux.lles polémistes pour trouver de nouvelles voix justes ? -->

Nous n'avons pas forcément besoin de nouveaux intellectuels mais de nouveaux polémistes.
Le récit et la séduction dans ce qu'elle a de ne pas nier l'existence de nos affects : AOC mais aussi (hélas) à droite les Zemmour, les plus efficaces sont les polémistes et raconteur.euses d'histoires.

Mathieu Magnaudeix, dans son livre sur la Génération AOC, parle d'une culture américaine, aux racines religieuses, pour parler de la croyance dans le pouvoir du récit personnel à animer d'autres personnes, notamment via des expériences vécues d'oppressions, souvent pour créer des liens entre un.e leader.euse et son public. (c'est aussi la même croyance qui conduit à parier sur des liens interpersonnels forts entre militant.es pour la constitution de collectifs robustes et dynamiques).

<!-- cf. https://www.contretemps.eu/gauche-ocasio-cortez-aoc-sanders-socialisme-usa/  -->

Considérer avec pragmatisme le problème que posent les médias en ligne vis-à-vis de leur fascination pour la stimulation de nos affects.

La concentration oligarchique des médias mainstream et le diktat de leurs sujets par le marché et les intérêts privés nous pousse à envisager la figure de porte-parole.
Via les talents de ses orateur-ices, avoir une vraie incarnation d'idées et d'affects.

C'est dans la tradition syndicale que l'on doit emprunter cet ensemble de pratiques constituée de formation permanente des militant-es (et non simplement des camps d'été) qui a permis l'émergence de figures politiques comme des député-es communistes d'origine ouvrière.
Cette formation inclurait aussi évidemment le sujet de l'apprentissage de l'éloquence, cette barrière éminemment de classe et qui a trait à la domination sociale tout autant qu'au hold-up de classes dominantes sur la culture, la pop culture comme la culture légitime (ou _culture générale_).

La maîtrise de la pop culture est un levier qui permet de toucher directement un public sur des médias en ligne, sans passer par le filtre à culture légitime que représentent les éditorialistes, qui adoubent Zemmour ou Finkielkraut parce qu'ils baragouinent sur les philosophes légitimes, et trouvent détestables la culture _youtuber_.

##### Une voix juste doit résonner avec son média et son public

La propagande n'est pas la caricature que l'on s'en fait d'un message répété jusqu'à l'écœurement et décliné sur tous supports. L'image plus stratégique qu'on se fait de la "communication politique" est d'ailleurs une simple version anoblie de celle-ci.

Le penseur Mark Fisher<!--, qui a inspiré Nick Srnicek, --> a déjà nommé le problème : [le **réalisme capitaliste**](https://entremonde.net/le-realisme-capitaliste), où le fait qu'on n'arrive pas conceptuellement à imaginer qu'il soit possible de sortir de la pensée hégémonique. Un optimisme reste malgré tout possible dans ces nouvelles narrations et récits : voir [_Le début de la fin du réalisme capitaliste ?_](https://www.contretemps.eu/fin-realisme-capitaliste/)

## Nous n'avons plus le temps : accélérer l'histoire avec Y. Citton

<!-- Ajout résumé Y. Citton sur médias -->

Comme si on avait peur d'essayer, ou que critiquer ça voudrait dire qu'il faut être irréprochble.

### C'est dur d'être sous le feu des projecteurs

Le moindre faux pas est exploité, les armées de trolls s'en donnent à cœur joie...
C'est pour ça qu'il faut une organisation derrière qui sache soutenir psychologiquement, matériellement et politiquement. Les faux-pas ça se damage-contrôle, si LREM le fait c'est faisable. Tout le monde a une mémoire de poisson rouge. Par exemple Camelia Jordana, même si elle venait à chuter ou à faire un _burn-out_, elle a donné le ton : sur les plateaux c'était elle qui scandait : "et pourquoi Castaner ne veut-il pas venir débattre ?"

Et non seulement ça broie mais il faut être talentueux-se. D'où l'écosystème à créer pour soutenir le peu de gens qui pourraient mais qui ne font pas en l'état actuel pour x ou y raison.
Avant, le Parti Communiste formait des ouvriers à devenir députés, maintenant le NPA forme toujours des militant-es sincères, qui ont vécu des choses personnellement, très souvent à travers des batailles syndicales, au feu des projecteurs : Philippe Poutou, Anasse Kazib plus récemment pour 2022.

### Quid de créer des leaders incontrôlables et narcissiques ?

Accepter le risque que ces porte-paroles prennent trop de pouvoir : c'est un risque de prendre du pouvoir tout court.

Après le problème c'est que tu crées des icones et il faut savoir pas se retrouver prisonniers d'elles, comme mélenchon qui prend trop de place même s'il a servi. par exemple le NPA qui dit on change besancenot par poutou pour pas en devenir prisonniers c'est vertueux, après c'est un compromis parce que j'aime bcp poutou mais je crois que besancenot etait très efficace, dans le genre colère froide impeccable et soutenue, avec les chiffres en tête et l'émotion. Même si Besancenot est encore invité et a su se rendre indispensable, et Poutou se doit d'être invité presque statutairement, ce qui ouvre peut-être la voix à la multiplication de figures médiatiques. Car eux aussi ont besoin de lui hein ils reinviteront, Besancenot réinvité moins d'un mois après aveoir été "insolent" sur BFM.

## gros travail de curation de vidéos efficaces, à repasser sur certains thèmes selon l'actualité

Y avait eu une intervention briiillante de besancenot y'a 6 mois, même sur le moment elle était hyper dure à trouver, genre la télé où ça passait le mettait ni sur son site ni sur youtube fallait la trouver parce que quelqu'un l'avait mise sur facebook. Des interventions brillantes y en a des tas mais il faut une mise en avant plus sytématique.

## On manque juste de gens

Il faut des figures fortes qui savent parler et réagir même dans des polémiques. Genre qui expriment notre réaction médusée qui trouve même pas les mots tellement tout est "trop".

Il aut qu'il y ait des voix qui disent qu'il n'y a pas que des Darmanin, des Zemmour et des boomers bourgeois qui le soutiennent sinon on devient fou.

## Développer l'éloquence politique à gauche

- Faire des concours d'éloquence politique pas en mode bourge mais en mode plus proche du parler-vrai, voire de la télé
- ou en tout cas favoriser l'émergence d'une culture de porte-paroles de qualité dans les organisations de gauche
- compléter par des formations au media-training

Le but est de repérer les gens capables d'évoquer des récits forts.

## Diversification selon les thématiques

Viser un placement dans les médias comme "voix" (parfois diviser par thème ? ou en tant que représentant-es d'organisations "faux nez" genre asso d'usagers ?)

Eviter qu'une personne se prononce sur les sujets de drug policy ou d'islam et se fasse antagoniser par l'extreme-droite et scontinue à être audible sur par exemple l'économie ?
aussi cool pour décentralisation du pouvoir et répond à la demande de "spécialistes" par les médias) (et quand même faire que ces personnes soient alignées sur l'ensemble des sujets du mvmt/parti/etc).

L'exemple du site féministes lesexpertes, ou chez les conservateurs le site d'un think tank qui permet de contacter un conservateur sur n'importe quel avis h24.

<!-- (source : livre sur aaron swartz) -->

<!-- ### Exemples Profs / masterclass

- ☐ L'économiste-à-Davos-like là (anglais)
- ☐ Besancenot
- ☐ Poutou
- ☐ Edouard Louis
- ☐ Lagasnerie ?
- ☐ Aissa Traoré
- ☐ Jerome Rodrigues ?
- ☐ FlyRider ?
- ☐ Guillaume Meurice ?
- ☐ l'ex good year ou metalor syndicaliste qui fait du théâtre là
- ☐ Clément Vikorovitch
- ☐ Hadama Traoré
- ☐ La meuf de la coordination étudiante qui condmnait pas la violence
  mdr
- ☐ Juliette Rousseau -->
<!--
[[media]]

Voir [[plan]] -->

<!-- ## notes leo

quelqu'un qui reflechit vite de facon spontanée pour analyser les discours, qu'il n'y ait pas que trop tribun 3e république Melenchon d'un côté, Poutou, et les intellos chelou genre Lagasnerie.

- Style trop soutenu / tordu
- c'est quoi les organisations/milieux qui permettraient de faire naître une culture comme ça ?
- Pierre : il te convainc avec le temps en froncant les sourcils quand tu dis des trucs et peu à peu tu te remets en question, sans t'orienter trop : dire ce qui est cringe et ce qui est cool -->
