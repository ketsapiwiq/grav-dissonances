---
title: 'La tactique du think-tank'
modified: 1603858371
---

<!-- ## Pourquoi un think tank et quoi dedans ? TL;DR -->

_**Cet article est encore un brouillon.**_

En fait en lisant Lucien Sfez sur "Technique et Politique", l'idée de stratégie "think tank expert pour pousser des politiques radicales", c'est simplement la _traduction_ d'un but politique dans le langage de l'idéologie-religion de l'occident : la Décision, le decisionnel, technocratique, souverain et toujours le récit du progrès atteint via la décision et l'expertise. C'est juste avouer qu'on peut pas changer cette idéologie de la Décision comme ça, qu'il faut travailler avec.

## Objectifs

- Rechercher la crédibilité "experte" et "objective" permet de cadrer les avis radicaux sur des problèmes politiques sous un prisme plus favorable
- car le langage "expert" des dominants est un instrument pour traduire, décrire et propager des idées émancipatrices à travers différents milieux
- via des contre-récits séduisants et des normes, lois et institutions désirables
- en imitant la démarche du think tank [_Autonomy_](https://autonomy.work)

## Quoi ?

5 axes d'un supposé think tank :

1.  reflexion sur les média comme 4e pouvoir : l'écologie de l'attention
2.  gratuité du logement ou propriété d'un seul logement pour tou-tes : Friot sur le fait d'étendre le communisme
3.  salaire/revenu universel : inconditionnel, qui ne réduit pas l'AAH, un ou une économiste pas sectaire, par création monétaire et/ou à faire payer aux riches
4.  emplois écolos : redéfinition de ce qu'est un salaire juste dans tous les emplois existants
5.  alternative à police et justice : police non armée et qui est composée de corps citoyen qui tourne, réduite à son minimum quand cas critiques et à du taf de bureau, remplacée par psys et assistantes social-es. Justice de réhabilitation.


# La tactique du think-tank

## Accélérer la diffusion de projets contre-hégémoniques et imposer sa définition du futur

Yves Citton, spécialisé en _media studies_, parle d'économie de l'attention. En commentant le _manifeste accélérationniste_, Citton exprime son opinion orientée média sur la stratégie politique :

> « Nous avons besoin de **construire une réforme à grande échelle des médias**. En dépit de l'apparente démocratisation permise par Internet et les réseaux sociaux, les médias traditionnels continuent à jouer un rôle crucial dans la sélection et dans le cadrage des récits, de même que dans l'accès aux financements permettant de mener un journalisme d'investigation. Pousser ces corps aussi près que possible d'un contrôle populaire est une tâche cruciale pour qui entend s'attaquer à la présentation actuelle de l'état des choses. […] Il est simplement hallucinant (et indéfendable) de voir les manifestations de gauche continuer à se promener tranquillement entre République et Bastille, alors que **c'est en bloquant le bâtiment de TF1 ou le téléjournal de France 2 qu'on pourrait contribuer bien plus directement à accélérer l'Histoire et son storytelling** [...]"

[_Accélérer la gauche écologiste ?_, Yves Citton, Multitudes n°56](http://www.multitudes.net/accelerer-la-gauche-ecologiste%E2%80%89/)

On retrouve cette sensiblité au fait que les médias mainstream sont d'énormes puissances politiques du fait qu'ils contrôlent les imaginaires et les représentations de millions de gens, extrêmement perméables aux récits créés pour l'intérêt de logiques capitalistes les plus crues, sans presque aucun contrôle démocratique.

### « Pour "sauver la planète", il faudrait ne travailler que 7 à 8 heures par semaine. »

Il y a un an, le [think-tank Autonomy](http://autonomy.work/), qui consacre ses réflexions à la place du travail dans notre société, ses crises et son avenir, [publiait un rapport](https://mrmondialisation.org/pour-eviter-lemballement-climatique-travaillez-deux-jours-par-semaine-maximum/) qui expliquait que le niveau de pollution de l'activité économique était tel que, **pour "sauver la planète", il faudrait ne travailler que 7 à 8 heures par semaine.**

Pour obtenir ce résultat, le rapport d'une douzaine de pages calculait un ratio entre un niveau de pollution "désirable" pour rester sous les 2° C de réchauffement et la productivité, dans trois pays, la France, le Royaume-Uni et l'Allemagne. Si les détails du calcul vous intéressent, [un article sur Mr Mondialisation](https://mrmondialisation.org/pour-eviter-lemballement-climatique-travaillez-deux-jours-par-semaine-maximum/) résume bien le contenu de ce rapport.

Ce rapport s'est retrouvé dans [Cosmopolitan](https://www.cosmopolitan.fr/pour-sauver-la-planete-il-ne-faudrait-travailler-que-5-heures-30-par-semaine,2027716.asp), ainsi que dans [l'édition du soir de Ouest-France](https://www.ouest-france.fr/leditiondusoir/data/51482/reader/reader.html#!preferred/1/package/51482/pub/74719/page/8), le [quatrième site de presse le plus visité en France](https://www.acpm.fr/Les-chiffres/Frequentation-internet/Sites-Grand-Public/Classement-unifie). Un article des Échos, plutôt de droite libérale, [prend d'ailleurs note](https://www.lesechos.fr/idees-debats/cercle/les-salaries-de-general-electric-premieres-victimes-de-la-transition-ecologique-1025923), sans trop de recul, que le rapport pousse à conclure que « la transition écologique rend caduc [le] modèle [de la croissance] et oblige à penser à la planète avant de penser à soi, d’envisager un mode de vie collectif et non plus individuel. ». Les conclusions de ce même rapport ont aussi été repris plus longuement et classiquement par [le Guardian](https://www.theguardian.com/environment/2019/may/22/working-fewer-hours-could-help-tackle-climate-crisis-study), et il a même fait la une du _Daily Star_, un tabloïd anglais.

![Le Daily Star titre sur le rapport d'Autonomy](../images/01C.la-communication-politique-la-traduction-legale/D8cBn1nWkAAXcEd.jpg)

Le rapport semble simpliste : comment organiser cette répartition du travail quand les métiers sont de plus en plus spécialisés et que les formes de travail sont de plus en plus diverses ? Quid des médecins dont on manque et qui font de très longues études ? Est-ce qu'avec une semaine de 9h, on ne polluerait pas plus avec nos loisirs sur notre temps libre ? Mais le but du rapport n'est peut-être pas tellement de répondre à toutes ces questions.

C'est marginal peut-être, mais plus important est comment le mec des Echos réutilise le rapport Autonomy sans connaître ses buts politiques.

## Accélérer le futur

Cette publication représente la concrétisation d'une idée promue par un petit groupe d'universitaires anglais, regroupés dans le think-tank _Autonomy_. Nick Srnicek notamment s'inspire des réflexions d'un collectif britannique étudiant étrange, le _Cybernetic Culture Research Unit_, pour développer une réflexion stratégique et pragmatique dans le combat contre l'hégémonie capitaliste. Il en tire des constats sur l'état actuel de la gauche socialiste et anticapitaliste, l'état d'esprit de ses militant·es, son imaginaire et ses modes d'action.

Deux figures majeures se cachent [derrière ce think-tank appelé « Autonomy »](http://autonomy.work/about-us/) : Nick Srnicek et Helen Hoster. Ces deux universitaires ont récemment influencé la pensée de gauche via deux manifestes provocateurs : Srnicek avec son _Manifeste accélérationniste_, qui dénonce la politique du _small is beautiful_ et le repli de la gauche vers les expérimentations politiques à petite échelle et le refus des institutions, et Hoster avec son [_Manifeste xénoféministe_](http://laboriacuboniks.net/fr/index.html), qui défend une posture hétérodoxe sur [l'écoféminisme](https://reporterre.net/Emilie-Hache-Pour-les-ecofeministes-destruction-de-la-nature-et-oppression-des), revendiquant les technologies comme possible source d'émancipation (un des slogans associés au xénoféminisme est "si la nature est injuste, changez la nature"). Ensemble, iels ont également co-écrit un livre intitulé _Après le travail : le combat pour le temps libre_, qui sert de référence centrale pour le think-tank Autonomy.

Mais c'est surtout dans un livre de Nick Srnicek de 2015 appelé _Accélérer le futur : Post-travail & post-capitalisme_ qu'est détaillée l'intuition qui semble avoir motivé la publication de ce rapport : il serait possible de rendre _mainstream_ des idées radicales de gauche, en basant sa stratégie sur la diffusion de récits qui en donneraient une lumière favorable.

## Imiter les think-tanks néolibéraux des années 80

On retrouve une partie de critique dans le livre récent de Geoffroy de Lagasnerie, "Sortir de l'impuissance politique", qui a beaucoup énervé dans la gauche radicale. Lui aussi évoque la Société du Mont-Pèlerin, organisation d'économistes à laquelle ont appartenu d'influents économistes libéraux comme Milton Friedman ou Friedrich Hayek, qui ont, des années 50 à leur triomphale victoire sous l'ère de Thatcher et Reagan, tenté d'influencer la politique, les médias et les courants universitaires pour rendre _mainstream_ un courant d'économie qui était minoritaire quand tout le monde se revendiquait keynésien, en essaimant patiemment leurs idées à travers une centaine de think-tanks d'orientation libérale et néolibérale dans le monde.


### _Folk politique_ et tabou de la stratégie : les programmes politique, et la grande échelle

Le positionnement de Srnicek est de ne pas laisser l'adversaire définir la pensée des futurs.

Dans ce livre, il estime qu'il serait désirable d'imiter, du moins en partie, la stratégie de la _Société du Mont-Pèlerin_, en visant à la fois médias, monde universitaire et influence sur les politiques publiques.

## Stratégie sur 20 ans

### La priorité subjective du hacking des médias par exemple : un luxe de classe intello privilégiée ? Mais n'est-ce pas toute la stratégie qui l'est ?

C'est aussi penser au long terme, au changement idéologique qui peut se passer sur 20 ou 30 ans.

**Se rendre compte qu'un changement structurel se fait sur 20 ans est un luxe de dominant. Les gens en souffrance et exploités doivent, elles et eux, lutter pour leur survie immédiate. Ca n'en reste pas un fait moins vrai que les universités et les centres de propagande, qu'elles soient à public "d'expert·es" (think tank) ou populaire (médias), influencent et façonnent lentement mais sûrement la société à An + 20.**

L'urgence de la situation nous fait donner le maximum de ce que peuvent les luttes en permanence. Ce qui conduit au burn-out militant : la grève générale et la manif doivent être déclarées partout tout le temps, l'effort (en temps, ressources matérielles mais aussi financières avec les caisses de grève) ne peut être retardé car l'urgence commande d'agir à fond maintenant et tout de suite, pour les gens qui souffrent, contre les conséquences chaque jour plus graves de l'extractivisme et de la pollution sur le climat. C'est vrai. Sauf que ça nous condamne à l'épuisement. C'est en cela que la stratégie est cynique, de droite, "pragmatique" : elle sacrifie certaines ressources, et dans le pire des cas, certaines personnes, pour un meilleur gain futur. Mais il n'y a rien de plus facile que de manœuvrer contre des gens sans stratégie et dont toutes les réactions sont prévisibles, car dictées sans médiation tactique par une morale transparente.


## "Sous le slashing, le meubling"

### La vulnérabilité structurelle de la presse _mainstream_ est due à sa précarité financière : faire de nécessité opportunité stratégique ?

Pour revenir à la diffusion étonnante du rapport Autonomy sur la semaine de 8h de travail, c'est dans un autre registre, celui du journaliste expérimenté, que Daniel Schneidermann se lamente de la vulnérabilité de la presse à cette superficialité :

> Vous connaissez le slashing ? Sinon, vous allez connaitre. C'est le fait de cumuler plusieurs boulots. Cette pratique "séduit de plus en plus", [c'est LCI qui le dit](https://www.lci.fr/emploi/le-slashing-seduit-de-plus-en-plus-un-francais-sur-trois-pret-a-cumuler-deux-emplois-etude-opinionway-horoquartz-2111479.html), photo à l'appui : il s'agit de "casser une routine, ou de naviguer dans des mondes différents". Bon. En réalité, c'est le quotidien sordide de salariés contraints de cumuler deux ou plusieurs jobs pour boucler les fins de mois. Pourquoi cette enquête sur le slashing ? Parce que LCI s'est laissé fourguer un sondage bidon commandé par un marchand de "solutions de gestion des temps et planning", nommé Horoquartz, [comme l'a relevé](https://twitter.com/peultier/status/1090294280999047168) mon excellent confrère de Libération, Frantz Durupt .
>
> La récurrence révèle assez que ce procédé "fait système", comme on dit : il existe un circuit, balisé, éprouvé, de recyclage de sondages biaisés en reportages "sociétaux", idéaux pour meubler le temps d'antenne, ou les pages des magazines, entre deux (vraies) pubs. La fake news est à la mode. Mais il ne faudrait pas oublier sa grande soeur, la junk news, providence de la presse paresseuse en mal de meubling.
>
> [Daniel Schneidermann, « Sous le slashing, le meubling »](https://www.arretsurimages.net/chroniques/le-matinaute/sous-le-slashing-le-meubling)

C'est aussi simplement que les journalistes sont précaires, souvent pigistes exploité-es, comme le montre le prix des piges exposé par le site [Paye Ta Pige](https://payetapige.com/), qui, à cause de la détérioration de leur métier, doivent souvent meubler avec des reprises de communiqués de presse vite .

Schneiderman démontre aussi qu'il y a surtout, sous ces nombreux exemples de titres un peu faciles passés dans plusieurs médias, la démonstration que la commande de sondages faisait partie de l'arsenal d'actions détournées permettant de faire de la publicité à un récit particulier que l'on souhaiterait communiquer.

Le rapport d'Autonomy sur la semaine de 8h ne contient que 12 pages ! Il est donc aisément reproductible par n'importe qui avec un peu d'expérience universitaire du format _paper_, et un peu de marketing numérique.

A l'inverse, le même think-tank avait pourtant pondu un rapport bien plus sérieux sur l'impact d'une semaine réduite à 4 jours de travail, de 96 pages. Mais, à part une mention dans The Guardian, il a plutôt été délaissé par le reste de la presse.

## Prémâcher le travail pour la presse via un travail sur le langage ? Le communiqué de presse

En visant la une, en facilitant des angles pour être repris dans les journaux papier, rédactions web et autres, il est nécessaire d'établir des éléments de langage, des clichés simples, et surtout de revêtir un habillage plutôt "neutre". Cela consiste à obéir à une suite de code du milieu journalistique et médiatique, où l'appartenance idéologique des auteur·ices et de la structure n'est pas revendiqué publiquement mais plutôt implicite, et où les codes visuels sont ceux du "think-tank" anglosaxon : supposément toujours "objectif" car composé "d'experts" qui ne font que "révéler ce que les chiffres ou les études disent".

Le site d'[autonomy.work](http://autonomy.work) possède effectivement une présentation assez institutionnelle, le thème pourrait d'ailleurs aisément être celui d'un cabinet d'affaires. La structure se présente comme étant « un think-tank indépendant et progressiste avec un seul focus : le travail ».

C'est utile la propagande !

<!-- http://www.ox.ac.uk/news/2018-02-21-social-media-and-internet-not-cause-political-polarisation -->

### Economie de l'attention : y participer ou promouvoir une écologie de l'attention ?

En attendant d'atteindre une _écologie de l'attention_, concept mis en avant par Yves Citton qui signifierait que nous portions collectivement l'attention aux choses telles qu'elles leur sont dues, nous sommes plutôt victimes des luttes sauvages pour notre fameux temps de cerveau disponible, dans une guerre économique qui s'appellerait économie de l'attention. De là, que faire ? Prôner par l'exemple une consommation ascétique de médias, ou au contraire livrer la bataille pour imposer des récits dans la cacophonie existante ?

<!-- Parler de l'absence de stratégie de Résistance à l'Agression Publicitaire malgré d'excellentes tactiques -->

#### Les multiples techniques de propagande : l'occupation de l'espace médiatique en tout genre et la création de l'actualité

**La structure _think-tank_ est un prétexte** : le cœur de l’objectif est la diffusion de récits.

La propagande institutionnelle, tristement, fonctionne : les think-tanks semblent encore jouir d’un grand prestige, mais peut-être est-ce plus judicieux de s’afficher en tant qu’association de concerné-es, en tant qu’ONG, en tant que média, en tant qu’acteur économique impacté, en tant que mouvement politique, en tant que particulier lambda…

# Fabriquer des narrations anticapitalistes efficaces

## Quelle communication des idées et images politiques pour des récits anticapitalistes efficaces ?

Le "bon sens" : c'est superficiel. A des buts de propagande, je considère (et les publicistes et lobbyistes avec moi) un peu qu'on peut faire faire de la gymnastique à n'importe quel raisonnement pour qu'il soit exprimé comme du "bon sens" (de façon plus ou moins subtile j'en conviens, mais la droite et l'extrême-droite sont très bien arrivés à [réutiliser Gramsci à des fins pratiques](http://www.slate.fr/story/130298/antonio-gramsci-explique)), je suis juste persuadé que :

1. il faut conquérir ce "bon sens" (équivalant peu ou prou à [la conquête de l'hégémonie culturelle](http://www.slate.fr/story/130298/antonio-gramsci-explique))
2. il y a des démonstrations qui sont plus ou moins efficaces pour que les gens soient convaincus que ton idéologie exprime quelque chose relevant du "bon sens"
3. c'est pourquoi je considère que dénicher des illustrations explicites de ces démonstrations dans le réel (puis les mettre au devant de la scène médiatique, mais c'est un autre sujet) est une tâche pertinente politiquement

## Que veut-on ?

1. Une culture de gauche qui apprend à créer et propager de meilleurs récits

2. Préciser et rendre crédibles des futurs alternatifs : le cadrage des problèmes politiques par des discours et récits, par différentes structures

3. Et défendu par des nouvelles voix : nous n'avons pas besoin de nouveaux intellectuels, mais peut-être de nouvelles polémistes, peut-être même des "figures morales", que l'on écoute car on ne les soupçonne pas de calculs. Le récit et la séduction dans ce qu'elle a de ne pas nier nos affects : AOC mais aussi (hélas) à droite les Zemmour, polémistes et raconteur-euses d'histoires.

- La puissance de la figure de l'expert-e et de l'expertise "neutre" : qu'en faire à part la critiquer ? les avantages d'une traduction en langage légal expert et légitime… que les publicitaires et autres spin-offs doctors de _think-tanks_ ont compris. A pragmatiquement réutiliser dans une entreprise de guerrilla médiatique et psychologique ?


La "communication (d'idées) politique" devrait être un objectif noble, et non quelque chose que l'on fuit par écœurement (bien que celui-ci soit compréhensible).

Des activistes américains proposent dans le guide _Beautiful Trouble_ (traduit en français par des gens d'ATTAC en _Joyeux Bordel : tactiques principes et théories pour faire la révolution_^[Bon alors si comme moi l'expression Joyeux Bordel vous fait sombrer dans la dépression en vous rappelant la dernière fois que vous avez vu des brigades de clown'activistes devant des CRS, ne vous enfuyez pas je suis de votre côté]) de [« faire le travail des médias à leur place » (en anglais)](https://beautifulrising.org/tool/do-the-medias-work-for-them).


### Réalisme capitaliste et affects dépressifs

Le penseur Mark Fisher, qui a inspiré Nick Srnicek, a déjà nommé le problème : [le **réalisme capitaliste**](https://entremonde.net/le-realisme-capitaliste), où le fait qu'on n'arrive pas conceptuellement à imaginer qu'il soit possible de sortir de la pensée hégémonique

 <img src="../images/01A.la-communication-politique-des-narrations-anticapitalistes/38444392_10155425286036577_6877143656315224064_o.jpg" alt="whydomillenialswanttodie" style="zoom:25%;" />



<!-- Insérer EXTRAIT [[media.folkpolitics.md]] -->

#### une non-esthétique militante : réflexions sur l'aspect répulsif de l'esthétique rouge-noir

On récupère les tares du maccarthysme et c'est comme militer avec une croix gammée sur la tête (cf.Contrapoints).

Comment expliquer quelque chose (comme pourquoi c'est foireux de se tourner vers des sauveurs individuels) aux gens de manière didactique sans utiliser de grands discours idéologiques (qui dans cet exemple seraient : "la pensée néolibérale est intrinsèquement individualiste et ne se focalise jamais sur l'aspect collectif du
qui est une phrase qui rebuterait trop de gens juste à cause de sa consonance).

C'est tiré d'un livre intitulé "Translating anarchy", dans lequel l'auteur explique que ce qu'ils ont fait à Occupy Wall Street, c'est traduire les concepts anarchistes en mots simples, sans jamais prononcer le mot "anarchisme", car cela rebute les gens.



### Sociologie de la traduction : mieux diffuser du militantisme dans d'autres milieux que la sphère militante.

### La théorie de la traduction : traductions entre légitimités idéologiques 

Créer ou rassembler des (vitrines d')organisations constituant une autorité dans certains milieux :

- autorité légale,
- religieuse (inclus toute une échelle de gens basant leur morale et éthique sur la foi),
- éconolmistes
- technicien-nes
- 

**Comme pour le marketing, vous devez segmenter/utiliser un vocabulaire et des moyens différents pour faire appel à des arguments moraux lorsque votre cible est segmentée, a une culture/codes culturels/vocabulaire/concepts/valeurs différents.**

#### Overton / incrementalisme

La doctrine politique derrière tout ça ne peut qu'être gradualiste, même si elle peut promouvoir des solutions radicales, elle cherche à modeler l'opinion.

Le gradualisme, ou l'incrémentalisme, doit être pleinement intégré à notre stratégie : pour tout interlocuteur, il faut mentionner un projet politique (petit ou grand) qui se situe à gauche de sa "fenêtre d'Overton".

Ce travail d'adaptation doit également supprimer une grande partie de la tradition qui fait défendre les positions radicales comme non négociables, complètes et pures.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Overton_Window_diagram.svg/800px-Overton_Window_diagram.svg.png" alt="fenêtre d'Overton" style="zoom:33%;" />

#### Le spectre des allié-es et la segmentation des publics
Le but est de rendre nos adversaires actifs des adversaires passifs, nos adversaires passifs des neutres, des neutres nos alliées passives et des alliérs passives nos alliées actives.

<img src="https://324tdg2wqb7v18kaji9r40f1-wpengine.netdna-ssl.com/wp-content/uploads/sites/4/2015/11/Spectrum-of-Allies-French.png" style="zoom:50%;" />

Recadrage moral : l'abandon de l'accord de Paris risque de faire envoyer des soldats US (parce que les guerres climatiques)
[[https://medium.com/truman-doctrine-blog/abandoning-the-paris-agreement-risks-troops-and-undermines-u-s-leadership-673fe16c05cc]](https://medium.com/truman-doctrine-blog/abandoning-the-paris-agreement-risks-troops-and-undermines-u-s-leadership-673fe16c05cc%5D%7B.underline%7D)

Pour convaincre que l'avortement doit forcément être gratuit tout le temps mieux vaut le faire avec pédagogie (et non condescendance) que de simplement faire les gros yeux et cet effort a une externalité sociétale extrêmement positive

> Parce que C'est précisément la définition du militantisme (qui demande une patience infinie à des gens qui n'en ont pas forcément la force, car le militantisme touche que des personnes qui en général sont directement touchées par les violences contre lesquelles elles militent, et donc les plus enclines à s'énerver contre des interlocuteurs qui nient ces violences par ignorance).
cf. article [le militantisme c'est chiant](http://lesquestionscomposent.fr/militer-cest-chiant/)

Le coût pédagogique de cette notion de lien avec des gens les amenant à la tolérance doit être mesuré.

Actuellement, l'esthétique de la gauche radicale actuelle, pleine de riot porn, de blagues sur le goulag, de débats abstraits et académiques, et de pure rage noire et rouge, est très facile à moquer et à ridiculiser. C'est difficile, car la gauche se bat quotidiennement contre l'apathie, les acteurs cruels et les situations déprimantes, mais comme le dit aussi une autre vidéo de Contrapoints, le public ne réfléchit pas avec une pure rationalité, il est beaucoup plus sensible à une image cool et sereine, "ice-cold cool" et nous n'avons pas d'autre solution que de faire cet effort de marketing, pédagogique, et peut-être un peu cynique.

### Quelle efficacité ?

Il faudrait nuancer ce qui fait qu'une telle tactique est efficace ou non. Il y a la question des différents publics : on peut vouloir faire pencher des modéré·es vers une idée plus radicale, influencer l'opinion de conservateurs radicaux pour les faire aller vers le centre, ou inciter des personnes passives à passer à l'action. Joshua Kahn Russell, de l'organisation Beautiful Trouble, parle du concept de [spectre des alliés](https://fr.trainings.350.org/?resource=spectrum-of-allies) pour évoquer cette stratégie de ciblage et d'influence de publics multiples. Étrangement, pour une même structure, la cohérence ne semble pas être un critère : Autonomy a publié à la fois un rapport évoquant la nécessité de passer la semaine à 4 jours et un autre pour la passer à 1 jour. Personne ne lui a reproché cette relative incohérence, car ce sont 2 publics différents qui étaient touchés et les deux angles pouvaient coexister. Le ton n'est en effet pas le même si l'on parle à des organisations sensibles au style institutionnel et froid, à des avocats sensibles au style juridique, ou pour la rubrique "Lifestyle" d'un magasine de mode...

#### …et une méthode pour collectivement trouver ces angles

1. Avoir une cause politique
2. Lire ce qu’il se dit dans les médias habituellement sur cette cause ou réfléchir à un événement en particulier sur lequel on est insatisfait des clichés véhiculés lorsque l’on couvre cet événement.
3. Essayer de trouver le « pourquoi » de cette couverture :
   1. Quel est l’événement qui en est la cause ? Qui est à l’origine de cet événement ?
   2. Pourquoi cette histoire est-elle facile à raconter ? Est-ce ce qu’on entend d’habitude ? A quel public s’adresse habituellement cette histoire ?
   3. Sur quelle vérité ou préjugé repose cette histoire ?
4. Qu’est-ce qui permettrait de faire tomber à l’eau cet angle ?
5. Quelle serait l’histoire qui le remplacerait et pourquoi son angle est-il meilleur pour le sujet abordé ?
   1. Comment pourrait-on échaffauder un événement pour suggérer cet angle aux éventuel-les journalistes présents ?
   2. Est-ce que cette histoire s’adresse au même public ? Par qui vise-t-on être repris ? Des médias mainstream ? Lesquels ? Chacun a un « style ».
   3. Est-ce rentable en termes de temps et d’effort pour l’impact recherché ?
   4. Quels compromis politiques aimerait-on faire si on ne trouve pas l’histoire parfaite à raconter mais quelque chose qui simplifie un peu ?
   5. Que pensent des tiers de cette histoire ? En particulier l’hypothétique public visé ? Comprennent-ils la même chose que nous ? Est-ce que ça les marque ? Est-ce qu’ils la jugent négativement ou l’estiment trop biaisée ou pas assez authentique et sincère pour considérer sa **vraisemblance** ? Il faut éviter tout ce qui permet à un public de **déconsidérer** notre histoire et nos informations : le public obéit à des règles comme celle de toujours éviter la création de dissonance cognitive (deux pensées contradictoires qui dévoilent notre incohérence d’action ou de pensée à propos d'un sujet).



## Illustration : la "taxe robot" de Hamon

La puissance du côté "taxe robot", quand tu le formules comme ça, à la place de "taxe sur le capital", l'éditocrate ne peut plus répondre sa réplique favorite "ça augmente le coût du travail" (la taxe sur le capital), parce qu'il devient inaudible pour un spectateur ("hein, mais on taxe un robot qui remplace un humain ça peut pas augmenter le coût du travail !"), et si l'éditorialiste parle d'amateurisme à cause de l'expression "taxe robot", on lui répond gentiment qu'en réalité il
s'agit bien d'un simple rebranding de l'idée "taxe sur le capital", mais que la formulation sémantique le prive de sa réplique "coût du travail", qui elle aussi de toute façon est diffusée principalement par des idéologues, en dehors de tout raisonnement scientifique. Pierre Cahuc et André Zyckenberg, derrière un verni économiste, sont par exemple des idéologues forcenés.

> Mark Bray, Translating Anarchy

> Sociologie de la traduction

- Comme le dit [Aaron Swartz dans un texte de 2009](https://crookedtimber.org/2009/08/04/toward-a-larger-left/), travail de simplification : que veut-on ? **Démocratie médiatique, démocratie économique (au travail), tout simplement.**

<!-- (Un tel travail de formule peut aussi se trouver sur toute cause...) -->

# La tactique : il faut donc _hacker_ cette attention médiatique et assumer de livrer la guerre de l'information. Comment ?

## Le pouvoir des récits : ne pas laisser le _storytelling_ au marketing

### L'élaboration commune de contre-récits séduisants

<!-- ## Quelques exemples… -->

Quels récits suggèrent donc des pistes ? L'exemple d'Autonomy prend à rebrousse-poil la question de la croissance, du chômage et de l'écologie : si le système ne peut tenir qu'avec une semaine de 6h, le plein emploi est tout trouvé. Exiger un salaire quand une mère s'occupe de ses enfants est un thème suffisamment grand public pour donner naissance à un début de débat.
Demander à [nationaliser Amazon (en anglais)](https://theoutline.com/post/6587/nationalize-amazon-make-bezos-our-bitch), voire mieux, "socialiser" par exemple Google et Facebook, pour les placer sous le contrôle économique et politique de tous leurs usagers, est un angle suffisamment provoc' pour faire parler de lui. Aux Etats-Unis, Alexandria Ocasio-Cortez a obtenu une couverture médiatique enviée de tou·te·s avec des mots d'ordre marquants tels que le "Green New Deal" ou encore la dissolution de _l'ICE_, une agence de police aux frontières se livrant à la chasse aux immigré·es et à leur enferment.


### Créer nos normes

 <!-- [[code+laws.des-labels-et-une-licence-copyfair.md]] -->

C'est aussi le travail des populistes de gauche de France Insoumise : un projet législatif précis pour contrecarrer le reproche que la gauche n'aurait "pas de solutions".

## Conclusion

Rechercher la crédibilité "experte" et "objective" permet de cadrer les avis radicaux sur des problèmes politiques sous un prisme plus favorable car le langage "expert" des dominants est un instrument pour traduire, décrire et propager des idées émancipatrices à travers différents milieux.
