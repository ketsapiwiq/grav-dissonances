---
title: 'Des récits anticapitalistes efficaces'
published: false
state: draft
priority: high
---

<!-- Voir [[plan.md]] -->

> TL;DR rationnaliser la diffusion politique de l'information et le véhicul de récits autour d'un programme politique détaillé et de mots d'ordre

> https://blog.mondediplo.net/pour-un-communisme-luxueux : il arrive à ne pas parler d'écologie !

<!-- - [Partie 1 / La tactique du think-tank](media.thinktank.md) -->

<!-- [[thinktank]] -->

<!-- - **Partie 2 / Méthode pour fabriquer des narrations anticapitalistes efficaces**

## Voir [[plan]] -->

<!-- [TOC] -->

- [Fabriquer des narrations anticapitalistes efficaces](#fabriquer-des-narrations-anticapitalistes-efficaces)
  - [Quelle communication des idées et images politiques pour des récits anticapitalistes efficaces ?](#quelle-communication-des-idées-et-images-politiques-pour-des-récits-anticapitalistes-efficaces-)
  - [TL:DR; Que veut-on ?](#tldr-que-veut-on-)
  - [Introduction](#introduction)
  - [Que faire ?](#que-faire-)
  - [Etat des lieux](#etat-des-lieux)
    - [Réalisme capitaliste et affects dépressifs](#réalisme-capitaliste-et-affects-dépressifs)
    - [Les raccourcis maladroits d'une "gauche passionnelle"](#les-raccourcis-maladroits-dune-gauche-passionnelle)
    - [La critique de la _folk politics_](#la-critique-de-la-folk-politics)
      - [Qui conduit à une non-esthétique militante : réflexions sur aspect répulsif de l'esthétique rouge-noir + examples](#qui-conduit-à-une-non-esthétique-militante--réflexions-sur-aspect-répulsif-de-lesthétique-rouge-noir--examples)
    - [Les objectifs xénoféministes](#les-objectifs-xénoféministes)
      - [Sortir de la folk politique, réconcilier volontés hégémoniques et infrapolitique : l](#sortir-de-la-folk-politique-réconcilier-volontés-hégémoniques-et-infrapolitique--l)
          - [ox17](#ox17)
          - [0x18](#0x18)
  - [Que faire ?](#que-faire--1)
    - [Sociologie de la traduction : mieux diffuser du militantisme dans d'autres milieux que la sphère militante.](#sociologie-de-la-traduction--mieux-diffuser-du-militantisme-dans-dautres-milieux-que-la-sphère-militante)
      - [Latour et Gramsci.](#latour-et-gramsci)
    - [La théorie de la traduction : traductions entre légitimités idéologiques différentes](#la-théorie-de-la-traduction--traductions-entre-légitimités-idéologiques-différentes)
      - [Like marketing, you have to segment/use different vocabulary and ways to call for moral arguments when your target is segmented, has a different culture/cultural codes/vocabulary/concepts/valeurs](#like-marketing-you-have-to-segmentuse-different-vocabulary-and-ways-to-call-for-moral-arguments-when-your-target-is-segmented-has-a-different-culturecultural-codesvocabularyconceptsvaleurs)
      - [Overton / incrementalism](#overton--incrementalism)
      - [Le spectre des allié-es et la segmentation des publics](#le-spectre-des-allié-es-et-la-segmentation-des-publics)
    - [A l'inverse : moral reframing, storytelling, marketing et démocratie, création de lexique et un ton ice cold motherf\*\*\*er](#a-linverse--moral-reframing-storytelling-marketing-et-démocratie-création-de-lexique-et-un-ton-ice-cold-motherfer)
      - [Ice cold motherf\*\*\*er : cf. [[Contrapoints]]](#ice-cold-motherfer--cf-contrapoints)
      - […et une méthode pour collectivement trouver ces angles (=> essay cotnrapoints + ouverture sur la traduction)](#et-une-méthode-pour-collectivement-trouver-ces-angles--essay-cotnrapoints--ouverture-sur-la-traduction)
  - [Conclusion](#conclusion)
    - [Quelle efficacité ?](#quelle-efficacité-)
    - [Créer nos normes](#créer-nos-normes)

# Fabriquer des narrations anticapitalistes efficaces

## Quelle communication des idées et images politiques pour des récits anticapitalistes efficaces ?

Le "bon sens" : c'est superficiel. A des buts de propagande, je considère (et les publicistes et lobbyistes avec moi) un peu qu'on peut faire faire de la gymnastique à n'importe quel raisonnement pour qu'il soit exprimé comme du "bon sens" (de façon plus ou moins subtile j'en conviens, mais la droite et l'extrême-droite sont très bien arrivés à [réutiliser Gramsci à des fins pratiques](http://www.slate.fr/story/130298/antonio-gramsci-explique)), je suis juste persuadé que :

1. il faut conquérir ce "bon sens" (équivalant peu ou prou à [la conquête de l'hégémonie culturelle](http://www.slate.fr/story/130298/antonio-gramsci-explique))
2. il y a des démonstrations qui sont plus ou moins efficaces pour que les gens soient convaincus que ton idéologie exprime quelque chose relevant du "bon sens"
3. c'est pourquoi je considère que dénicher des illustrations explicites de ces démonstrations dans le réel (puis les mettre au devant de la scène médiatique, mais c'est un autre sujet) est une tâche pertinente politiquement

## TL:DR; Que veut-on ?

1. Une culture de gauche qui apprend à créer et propager de meilleurs récits

2. Préciser et rendre crédibles des futurs alternatifs : le cadrage des problèmes politiques par des discours et récits, par différentes structures

3. [Et défendu par des nouvelles voix ](media.voixdissonantes.md): nous n'avons pas besoin de nouveaux intellectuels mais de nouveaux polémistes. Le récit et la séduction dans ce qu'elle a de ne pas nier nos affects : AOC mais aussi (hélas) à droite les Zemmour, polémistes et raconteur-euses d'histoires

---

- Face aux règles du jeu des médias mainstream et en ligne à l'ère de la société des flux accélérés d'images et de représentations, quels moyens d'actions prioriser, quelle stratégie ?

- La puissance de la figure de l'expert-e et de l'expertise "neutre" : qu'en faire à part la critiquer ? les avantages d'une traduction en langage légal expert et légitime… que les publicitaires et autres spin-offs doctors de _think-tanks_ ont compris. A pragmatiquement réutiliser dans une entreprise de guerrilla médiatique et psychologique ? (cf. [[media.thinktank.md]])

- **Conclusion : ** OK mais, avec qui ? jusqu'à où ? pour défendre quoi ? quel camp politique pour l'incarner ? un programme transitionnel ? Dual power ? [[laws.programme-transitoire-sexy.md]]

## Introduction

La "communication (d'idées) politique" devrait être un objectif noble, et non quelque chose que l'on fuit par écœurement (bien que celui-ci soit compréhensible).

Des activistes américains proposent dans le guide _Beautiful Trouble_ (traduit en français par des gens d'ATTAC en _Joyeux Bordel : tactiques principes et théories pour faire la révolution_^[Bon alors si comme moi l'expression Joyeux Bordel vous fait sombrer dans la dépression en vous rappelant la dernière fois que vous avez vu des brigades de clown'activistes devant des CRS, ne vous enfuyez pas je suis de votre côté]) de [« faire le travail des médias à leur place » (en anglais)](https://beautifulrising.org/tool/do-the-medias-work-for-them).

<!-- Passage slashing ? Meilleur exemple qu'article autonomy ? -->

<!-- Un exemple extrait de [[media.recits...]] -->

## Que faire ?

- pousser du matériel pédagogique pour dire au public qu'ils
  peuvent enfin expliquer quelque chose de précis dans leur entourage,
  cibler précisément es publics qui ne seront que relais intelligents,
  assureront la médiation de qualité (je veux convaincre mon oncle
  sur... mais je n'ai pas les argunets).
- ne pas participer à la surinformation sur des news horribles dont
  on a pas le ressort d'action quand on est déjà préocupé par un
  thème (quand on est déjà touché profondémebt par l'horreur avec
  lesmigrants, on n'en rajoute pas une couche, mais éventuellement on
  sensibilise les autres)
- le public est il un corps militant à uniformiser (+
  intersectionnel par exemple, moins antivaxx haha), ou un corps
  hétérogène plus large à recruter dans un premier cercle soft lus
  politisé ? (chaine Mr Mondialisation par exemple ? encore plus soft
  genre AJ+ france infos vidéos ?)

## Etat des lieux

<!-- ### Surcharge informationnelle et captation de l'attention par des monopoles -->

<!-- media.surcharge-informationnelle.md -->

<!-- [[media.surcharge-informationnelle.md]] -->

<!-- media.realismecapitaliste.md -->

### Réalisme capitaliste et affects dépressifs

<!-- … -->

Cf. [Why millenials make jokes about suicide](https://www.salon.com/2018/02/10/why-millennials-are-making-memes-about-wanting-to-die/) + https://www.upworthy.com/someone-asked-millennials-why-they-always-joke-about-dying-and-the-answers-were-pretty-serious

Cf. Mark Fisher

Le penseur Mark Fisher, qui a inspiré Nick Srnicek, a déjà nommé le problème : [le **réalisme capitaliste**](https://entremonde.net/le-realisme-capitaliste), où le fait qu'on n'arrive pas conceptuellement à imaginer qu'il soit possible de sortir de la pensée hégémonique

 <img src="../images/01A.la-communication-politique-des-narrations-anticapitalistes/38444392_10155425286036577_6877143656315224064_o.jpg" alt="whydomillenialswanttodie" style="zoom:25%;" />

La gauche des "affects tristes" de Macron

L'optimisme quand même ? https://www.contretemps.eu/fin-realisme-capitaliste/

### Les raccourcis maladroits d'une "gauche passionnelle"

<!-- Raccourcir -->

Chaque pensée très simplifiée, cliché le plus éculé, topos, trope, est un mème. "Les pauvres fraudent les allocs" est un mème, "les entrepreneurs créent de la croissance", "la croissance crée des emplois", mais aussi "...".

Je pose que le résultat des élections est le résultat d'une bataille d'influence sémantique autour des termes "emploi", "travail", "croissance", "austérité", etc.

**la clarté du rationnel au service du discours et non imposé à lui, les raccourcis maladroits d'une gauche passionnelle ("le mobilier antipauvre => le capitalisme tue") comme marque de fabrique de l'extrême-gauche, le côté dégoût du discours passionnel (vegan, promigrants, propauvres) pour son côté indécent face à la violence sourde qui broie et traverse
les corps,** le discours mélangeant rationnel et passionnel et pop culture qui se vante de catalyser l'ensemble des _libido sciendi_ de ses militant-es **devrait pourtant broyer n'importe quel EDL ministériel** par son impact culturel non ?

Medium is the message, bien sûr, car le tract et la contre-culture n'est pas un non-choix mais bien une autre culture avec d'autres codes excluants. C'est aussi une question d'ego, le discours passionnel est vu comme plus méprisant/paternaliste ou méprisable.

Conférences gesticulées d'éduc. pop. : « La force et la beauté de cette forme de représentation tiennent beaucoup à la juxtaposition de **son "contenu froid" -- le savoir théorique -- et de son contenu "chaud" -- la relation intime de la personne à son sujet.** Ces gens ont tous un rapport très fort à leur métier : l'identification et l'émotion fonctionnent donc très bien », détaille Vincent Eches.

Il y a un véritable style à adopter, sobre et élégant, consistant à ewposer du contenu froid de façon à être lus et digérés

### La critique de la _folk politics_

<!-- Insérer EXTRAIT [[media.folkpolitics.md]] -->

#### Qui conduit à une non-esthétique militante : réflexions sur aspect répulsif de l'esthétique rouge-noir + examples

On récupère les tares du maccarthysme et c'est comme militer avec une croix gammée sur la tête (cf.Contrapoints).

How to explain something (like why it's fucked up to turn to individual saviors) to people in a didactical way without using big ideological speeches (which in that example would be: "neoliberal thinking is inherently individualistic and never focuses on the collective aspect of
social progress!" which is a sentence that would turn off too many people just because of how it sounds).

It's from a book called "Translating anarchy", where the author says that what they basically did at occupy wall street was translating anarchist concepts into plain words without ever pronouncing anarchism because it turns people off.

### Les objectifs xénoféministes

#### Sortir de la folk politique, réconcilier volontés hégémoniques et infrapolitique : l

###### ox17

[...]
_De l’échelle mondiale à l’échelle locale, de la troposphère à nos corps, le xénoféminisme revendique sa responsabilité dans la construction de nouvelles institutions de proportions technomatérialistes hégémoniques. À l’instar d’ingénieurs qui devraient concevoir une structure d’ensemble ainsi que les éléments moléculaires qui la compose, XF insiste sur l’importance de la sphère mésopolitique à la fois contre l’efficacité limitée des actions locales, de la création de zones autonomes et de l’horizontalisme absolu, ainsi que contre toute tentative d’imposer des valeurs et des normes en s’instituant comme autorité supérieure ou en prônant une quelconque transcendance. L’arène mésopolitique des ambitions universalistes du xénoféminisme se comprend comme un réseau mobile et intriqué de lignes de transit entre ces polarités. En pragmatistes, nous appelons à la contamination comme à un moteur de mutation entre de telles frontières._

###### 0x18

_XF affirme qu’adapter notre comportement à la complexité prométhéenne de l’ère actuelle est un travail qui requiert de la patience, mais une patience acharnée qui n’a rien de l’ « attente ». Calibrer une hégémonie politique ou un méméplexe séditieux implique non seulement la création d’infrastructures matérielles pour rendre explicites les valeurs que portent ces organismes, mais impose aussi certaines exigences aux sujets que nous sommes. Comment allons-nous habiter ce nouveau monde ? Comment construire un meilleur parasite sémiotique – un parasite qui suscitera les désirs que nous voulons désirer, et qui orchestrera non pas une orgie autophage d’indignité ou de colère, mais une communauté égalitaire et émancipatrice soutenue par de nouvelles formes de solidarité désintéressée et de maîtrise de soi collective ?_

## Que faire ?

### Sociologie de la traduction : mieux diffuser du militantisme dans d'autres milieux que la sphère militante.

#### Latour et Gramsci.

> Ou pourquoi il faut pas avoir un ton qui fait réf à des trucs marxistes chelous à la NPA dans une brochure pour des parents cathos sur leur fils/fille qu'ils pensent homosexuel(le), c'est pas efficace en fait ( du coup c'est le dsicours folklore catho, et donc l'idée de MORAL REFRAMING)

capacité à traduire des concepts de façon trans-sociétal (plein de groupes aux intérets et codes culturels différents unis par leur point commun de la manipulation et compréhensionde la "langue" juridique) + capacité à posséder une puissance symbolique 'd'expertise', de rationnalité voire de fausse neutralité (car le "milieu" culturel des acteurs du jruidique est un milieu avec du pouvoir ?)

### La théorie de la traduction : traductions entre légitimités idéologiques différentes

Du coup parler des légitimités idéologiques différentes que sont légale, religieuse, etc. Rephraser ce que j'ai compris de la théorie ANT, qu'il y a des liens entre un binôme de deux idées ou opinions appartenant à deux modes d'existence différent. Par exemple en cartographie de la controverse, certaines théories sur par exemple le réchauffement climatique ou l'avortement,

Créer ou rassembler des (vitrines d')organisations constituant une autorité chez chaque légitimité :

- légale,
- religieuse (inclus tout une échelle de gens basant leur morale et éthique sur la foi, jusqu'au dogmatisme absolu),
- économique (monde, Etat, entreprise, foyer, individu),
- politique du foyer (anarchisme)

n'est-ce pas cela que Latour nomme _modes d'existence_ dans son _Enquête sur les modes d'existence_ et qui seraient au nombre de 16 ?

<!-- Sois plus sûr de toi, Hadrien, oui c'est le cas) -->

- mais aussi, paradoxalement, dans le mode d'existence de la loi, du droit et de l'autorité légale.

#### Like marketing, you have to segment/use different vocabulary and ways to call for moral arguments when your target is segmented, has a different culture/cultural codes/vocabulary/concepts/valeurs

Pour la #cartographie, la raccorder à chaque #policies sur le principe que pr chaque policies il y a qq chose à faire chez elleux pr les
pousser dans le camembert suivant du spectrum of allies, et à quel stade d'overton window est telle orga pour telle idée, et le positionnement
8values de chaque orga et de chaque idée (avec N/A sí pas pertinent).
Faire un court essai sur ce framework orgas/policies/overton/spectrumofallies/8values.
Pour les #argumentaires les seules coordonnées nécessaires sont le spectrum of allies non? (et l'efficacité sur un public cible borné dans
les coordonnées 8values?)

<!-- #argumentaires #cartographie #policies -->

#### Overton / incrementalism

<!-- C'est là où on dit que la doctrine politique derrière tout ça ne peut qu'être gradualiste, même si elle peut promouvoir des solutions radicales, elle cherche à modeler l'opinion -->

Gradualism, or incrementalism, needs to be fully integrated into our strategy: for any interlocutor, mention should be made of a political project (small or large) that is to the left of its "Overton window".

This work of adaptation also needs to remove much tradition that makes defend radical positions as non-negotiable, full and pure.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Overton_Window_diagram.svg/800px-Overton_Window_diagram.svg.png" alt="fenêtre d'Overton" style="zoom:33%;" />

#### Le spectre des allié-es et la segmentation des publics

(dans essai B ou petit paragraphe ici quand même ?)

<img src="https://324tdg2wqb7v18kaji9r40f1-wpengine.netdna-ssl.com/wp-content/uploads/sites/4/2015/11/Spectrum-of-Allies-French.png" style="zoom:50%;" />

### A l'inverse : moral reframing, storytelling, marketing et démocratie, création de lexique et un ton ice cold motherf\*\*\*er

Moral reframing : abandoning the paris agreement risks troops (parce que climate wars)
[[https://medium.com/truman-doctrine-blog/abandoning-the-paris-agreement-risks-troops-and-undermines-u-s-leadership-673fe16c05cc]](https://medium.com/truman-doctrine-blog/abandoning-the-paris-agreement-risks-troops-and-undermines-u-s-leadership-673fe16c05cc%5D%7B.underline%7D)

- Pour convaincre que l'avortement doit forcément être gratuit tout le temps mieux vaut le faire avec pédagogie (et non condescendance) que de simplement faire les gros yeux et cet effort a une externalité sociétale extrêmement positive

- Narrativas positivas : se pueden cambiar las cosas (y la gente)

  > parce que C'est précisément la définition du militantisme (qui demande une patience infinie à des gens qui n'en ont pas forcément la force, car le militantisme touche que des personnes qui en général sont directement touchées par les violences contre lesquelles elles militent, et donc les plus enclines à s'énerver contre des interlocuteurs qui nient ces violences par ignorance),
  > comme défini dans l'article du très bon blog qui parle de Poire.
  >
  > cf. article [le militantisme c'est chiant](http://lesquestionscomposent.fr/militer-cest-chiant/)

- Le coût pédagogique de cette notion de lien avec des gens les amenant à la tolérance doit être mesuré.

Reparler du sensible évoqué dans le texte sur dissonance cogntiive et folk politics

#### Ice cold motherf\*\*\*er : cf. [[Contrapoints]]

Currently, the aesthetics of the current radical left, full of riot porn, gulag jokes, abstract and academic debates, and pure black and red rage, is very easy to mock and ridicule. It is difficult, because the left fights daily against apathy, cruel actors and depressing situations, but as another Contrapoints video also says, the public does not reflect with pure rationality, it is much more sensitive to a cool and serene image, "ice-cold cool" and we have no other solution than to make this marketing effort, pedagogical, and perhaps a little cynical.

#### …et une méthode pour collectivement trouver ces angles (=> essay cotnrapoints + ouverture sur la traduction)

1. Avoir une cause politique
2. Lire ce qu’il se dit dans les médias habituellement sur cette cause ou réfléchir à un événement en particulier sur lequel on est insatisfait des clichés véhiculés lorsque l’on couvre cet événement.
3. Essayer de trouver le « pourquoi » de cette couverture :
   1. Quel est l’événement qui en est la cause ? Qui est à l’origine de cet événement ?
   2. Pourquoi cette histoire est-elle facile à raconter ? Est-ce ce qu’on entend d’habitude ? A quel public s’adresse habituellement cette histoire ?
   3. Sur quelle vérité ou préjugé repose cette histoire ?
4. Qu’est-ce qui permettrait de faire tomber à l’eau cet angle ?
5. Quelle serait l’histoire qui le remplacerait et pourquoi son angle est-il meilleur pour le sujet abordé ?
   1. Comment pourrait-on échaffauder un événement pour suggérer cet angle aux éventuel-les journalistes présents ?
   2. Est-ce que cette histoire s’adresse au même public ? Par qui vise-t-on être repris ? Des médias mainstream ? Lesquels ? Chacun a un « style ».
   3. Est-ce rentable en termes de temps et d’effort pour l’impact recherché ?
   4. Quels compromis politiques aimerait-on faire si on ne trouve pas l’histoire parfaite à raconter mais quelque chose qui simplifie un peu ?
   5. Que pensent des tiers de cette histoire ? En particulier l’hypothétique public visé ? Comprennent-ils la même chose que nous ? Est-ce que ça les marque ? Est-ce qu’ils la jugent négativement ou l’estiment trop biaisée ou pas assez authentique et sincère pour considérer sa **vraisemblance** ? Il faut éviter tout ce qui permet à un public de **déconsidérer** notre histoire et nos informations : le public obéit à des règles comme celle de toujours éviter la création de dissonance cognitive (deux pensées contradictoires qui dévoilent notre incohérence d’action ou de pensée à propos d'un sujet).

## Conclusion

### Quelle efficacité ?

Il faudrait nuancer ce qui fait qu'une telle tactique est efficace ou non. Il y a la question des différents publics : on peut vouloir faire pencher des modéré·es vers une idée plus radicale, influencer l'opinion de conservateurs radicaux pour les faire aller vers le centre, ou inciter des personnes passives à passer à l'action. Joshua Kahn Russell, de l'organisation Beautiful Trouble, parle du concept de [spectre des alliés](https://fr.trainings.350.org/?resource=spectrum-of-allies) pour évoquer cette stratégie de ciblage et d'influence de publics multiples. Étrangement, pour une même structure, la cohérence ne semble pas être un critère : Autonomy a publié à la fois un rapport évoquant la nécessité de passer la semaine à 4 jours et un autre pour la passer à 1 jour. Personne ne lui a reproché cette relative incohérence, car ce sont 2 publics différents qui étaient touchés et les deux angles pouvaient coexister. Le ton n'est en effet pas le même si l'on parle à des organisations sensibles au style institutionnel et froid, à des avocats sensibles au style juridique, ou pour la rubrique "Lifestyle" d'un magasine de mode...

<!-- ## Exemples

EXTRAIT [[media.recits.exemples.md]] -->

### Créer nos normes

 <!-- [[code+laws.des-labels-et-une-licence-copyfair.md]] -->

les labels, Normes de Li-fi et de FPGA à la stman mais aussi le label low tech. Move classique de PR de financer un label qui félicite ses produtis.

<!-- ---

# Chutes

[[media.recits.99.chutes.md]] -->
