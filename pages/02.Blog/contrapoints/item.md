---
title: 'Contrapoints, conquête de YouTube et communication politique'
media_order: 'contra1.png,chelseasaunders-idw.png,contra-mosaique-0256410.png,hailsatan.png,contra2.png,DKh70IBX0AAjGTm-683x1024.jpg,DKh72i7WAAAiAdP-683x1024.jpg,DKh73TEX0AEfAjW-683x1024.jpg,800px-Overton_Window_diagram.svg-20200326185434268.png,Spectrum-of-Allies-French.png,image-1024x467.png,rhino-1024x573.png,quoichanger-2-1024x573.png,goldenpizza.png'
date: '04-06-2020 02:07'
taxonomy:
    tag:
        - contrapoints
        - 'communication politique'
redirect: 'https://eunomia.media/2020/06/05/contrapoints/'
---

## [Contrapoints, conquête de YouTube et communication politique](https://www.assoeunomia.fr/2020/06/05/contrapoints/)

Ça n’a pas l’air de s’arranger : vous avez vu vos (ex-)ami·es centristes commencer à réutiliser des termes comme « grand remplacement », « féminazi »… qui n’étaient, vous le jureriez, qu’utilisés par votre petit cousin fan du Raptor Dissident à peine quelques mois plus tôt (oui, celui là-même qui s’est acheté le bouquin d’Alain Soral et fait partie des gens qui répèteront que « l’Empire » juif est derrière toutes les guerres).

N’y aurait-il que l’extrême-droite pour déjouer insidieusement les règles du débat et persuader de leurs idées un public grandissant ?

![](contra1.png)